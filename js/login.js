'user strict';

if(localStorage.getItem('logged_user') != ''){
    $(location).attr('href', '../templates/products.html');
}

$("#login-btn").click(function(e) {
    e.preventDefault();

    let username = $("#username").val();
    let password = $("#password").val();

    let users = JSON.parse(localStorage.getItem('users'));

    let found_user = false;

    users.forEach(user => {
        if(user.username == username) {
            found_user = true;
            $("#username-error").css("display", "none");
            if(!(password===user.password)){
                $("#pass-error").css("display", "block");
            }else{
                $("#pass-error").css("display", "none");
                $("#login-form").remove();
                localStorage.setItem('logged_user', username);
                $("#signup-success").css("display", "block");
            }
        }    
    });

    console.log(found_user);
    if(!found_user) {
        $("#pass-error").css("display", "none");
        $("#username-error").css("display", "block");
    }
});