"use strict";

const api_key = "https://api.jsonbin.io/b/5e3b9bf679afb813dc1aa7da";
const secret_key = "$2b$10$cSMB4gI4etuYQ.Cj8NblR..JU7M64iuvMpyq.x5r4.vGDzqI7sXsG";

if(localStorage.getItem('logged_user') == ''){
    $(location).attr('href', '../templates/login.html');
} else {
    var users = JSON.parse(localStorage.getItem('users'));
    var logged_user = localStorage.getItem('logged_user');

    users.forEach(user => {
        if(logged_user===user.username){
            if(!user.hasOwnProperty("is_admin") && user.is_admin !== "true"){
                $(location).attr('href', '../templates/products.html');
            }
        }
    });
}

function getData(country_name, sales_tax, callback){
    let req = new XMLHttpRequest();

    req.onreadystatechange = () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            var data = JSON.parse(req.responseText);
            callback(data, country_name, sales_tax);
        }
    };

    req.open("GET", api_key);
    req.setRequestHeader("secret-key", secret_key);
    req.send();
}

$("#product-add-btn").click(async function(e) {
    e.preventDefault();
    var country_name = $("#country-name").val();
    var sales_tax = $("#sales-tax").val();

    if( country_name == "" || sales_tax == "") {
        alert("Please entr all values");
    }else{
        var data = getData(country_name, sales_tax, saveData);
    }
})

async function saveData(data, country_name, sales_tax) {
    let req = new XMLHttpRequest();

    req.onreadystatechange = () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            $(location).attr('href', '../templates/country.html');           
        }
    };

    req.open("PUT", api_key, true);
    req.setRequestHeader("Content-type", "application/json");
    req.setRequestHeader("versioning", "false");
    req.setRequestHeader("secret-key", secret_key);

    data["countries"].push({
        country_name,
        sales_tax
    });
    
    req.send(JSON.stringify(data));
}