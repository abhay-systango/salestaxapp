"use strict";

const api_key = "https://api.jsonbin.io/b/5e3b9bf679afb813dc1aa7da";

if(localStorage.getItem('logged_user') == ''){
    $(location).attr('href', '../templates/login.html');
} else {
    var users = JSON.parse(localStorage.getItem('users'));
    var logged_user = localStorage.getItem('logged_user');

    users.forEach(user => {
        if(logged_user===user.username){
            if(!user.hasOwnProperty("is_admin") && user.is_admin !== "true"){
                $(location).attr('href', '../templates/products.html');
            }
        }
    });
}

let req = new XMLHttpRequest();

req.onreadystatechange = () => {
  if (req.readyState == XMLHttpRequest.DONE) {
    var data = JSON.parse(req.responseText);
    var countries = data["countries"];

    countries.forEach(country => {
        var name = country.country_name;
        var sales_tax = country.sales_tax;

        let ele = `<tr data-country="${name}" data-tax="${sales_tax}"><td>${name}</td><td>${sales_tax}</td></tr>`;
        $("#country-container").append(ele);
    });
  }
};

req.open("GET", api_key, true);
req.setRequestHeader("secret-key", "$2b$10$cSMB4gI4etuYQ.Cj8NblR..JU7M64iuvMpyq.x5r4.vGDzqI7sXsG");
req.send();

function getData() {
    let req = new XMLHttpRequest();

    req.onreadystatechange = () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            return JSON.parse(req.responseText);
        }
    };

    req.open("GET", api_key, true);
    req.setRequestHeader("secret-key", "$2b$10$cSMB4gI4etuYQ.Cj8NblR..JU7M64iuvMpyq.x5r4.vGDzqI7sXsG");
    req.send();
}

$("#update").on('click', function() {
    var countries = [];
    $("#country-container tr").each(function() {
        var country_name = $(this).attr("data-country");
        var sales_tax = $(this).attr("data-tax");
        countries.push({country_name, sales_tax});
    });
    console.log(getData());
    console.log(countries);
});