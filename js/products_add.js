"use strict";

const api_key = "https://api.jsonbin.io/b/5e3b9bf679afb813dc1aa7da";
const secret_key = "$2b$10$cSMB4gI4etuYQ.Cj8NblR..JU7M64iuvMpyq.x5r4.vGDzqI7sXsG";

if(localStorage.getItem('logged_user') == ''){
    $(location).attr('href', '../templates/login.html');
}

$("#logout-btn").click(function(e) {
    alert();
    e.preventDefault();
    localStorage.setItem('logged_user', '');
    $(location).attr('href', '../templates/login.html');
});

// Getting data from API
let req = new XMLHttpRequest();

req.onreadystatechange = () => {
  if (req.readyState == XMLHttpRequest.DONE) {
    var data = JSON.parse(req.responseText);
    var countries = data["countries"];

    countries.forEach(country => {
        var country_name = country.country_name;
        $("#country").append(`<option value="${country_name}">${country_name}</option>`)
    });
  }
};

req.open("GET", api_key, true);
req.setRequestHeader("secret-key", "$2b$10$cSMB4gI4etuYQ.Cj8NblR..JU7M64iuvMpyq.x5r4.vGDzqI7sXsG");
req.send();

function getData(name, price, country, callback){
    let req = new XMLHttpRequest();

    req.onreadystatechange = () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            var data = JSON.parse(req.responseText);
            callback(data, name, price, country);
        }
    };

    req.open("GET", api_key);
    req.setRequestHeader("secret-key", secret_key);
    req.send();
}

$("#product-add-btn").click(async function(e) {
    e.preventDefault();
    var name = $("#product-name").val();
    var price = $("#product-price").val();
    var country = $("#country").val();

    if( name == "" || price == "" || country == 0) {
        alert("Please entr all values");
    }else{
        var data = getData(name, price, country, saveData);
    }
})

async function saveData(data, name, price, country) {
    let req = new XMLHttpRequest();

    req.onreadystatechange = () => {
        if (req.readyState == XMLHttpRequest.DONE) {
            $(location).attr('href', '../templates/products.html');           
        }
    };

    req.open("PUT", api_key, true);
    req.setRequestHeader("Content-type", "application/json");
    req.setRequestHeader("versioning", "false");
    req.setRequestHeader("secret-key", secret_key);

    data["products"].push({
        name,
        country,
        price
    });
    
    req.send(JSON.stringify(data));
}