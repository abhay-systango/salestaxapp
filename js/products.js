"use strict";

const api_key = "https://api.jsonbin.io/b/5e3b9bf679afb813dc1aa7da";

if(localStorage.getItem('logged_user') == ''){
    $(location).attr('href', 'file:///home/ulap104/Desktop/work/salestaxapp/templates/login.html');
} else {
    var users = JSON.parse(localStorage.getItem('users'));
    var logged_user = localStorage.getItem('logged_user');

    users.forEach(user => {
        if(logged_user===user.username){
            if(user.hasOwnProperty("is_admin") && user.is_admin === "true"){
                $('#navbar').append('<li class="nav-item active"><a class="nav-link" href="./country.html">Country</a></li>');
            }else{
                console.log(false);
            }
        }
    });
}

// Getting data from API
let req = new XMLHttpRequest();

req.onreadystatechange = () => {
  if (req.readyState == XMLHttpRequest.DONE) {
    var data = JSON.parse(req.responseText);
    var products = data["products"];
    var countries = data["countries"];

    products.forEach(product => {
        var price = parseInt(product.price);
        var sales_tax = parseInt(getCountryDetail(countries, product.country));
        var product_tax = (price*sales_tax)/100;
        var finalPrice = price + product_tax;

        let ele = `<tr><td>${product.name}</td><td>${product.country}</td><td>${product.price}</td><td>${sales_tax}</td><td>${finalPrice}</td></tr>`;
        $("#products-container").append(ele);
    });
  }
};

req.open("GET", api_key, true);
req.setRequestHeader("secret-key", "$2b$10$cSMB4gI4etuYQ.Cj8NblR..JU7M64iuvMpyq.x5r4.vGDzqI7sXsG");
req.send();

function getCountryDetail(countries, searchCountry) {
    for (const country of countries) {
        if(country.country_name.toLowerCase() == searchCountry.toLowerCase()){
            return country.sales_tax;
        }
    }
}