'user strict';

if(localStorage.getItem('logged_user') != ''){
    $(location).attr('href', '../templates/products.html');
}

$("#signup-btn").click(function(e) {
    e.preventDefault();

    let username = $("#username").val();
    let password = $("#password").val();
    let re_password = $("#re-password").val();

    if(!(password===re_password)){
        $("#pass-error").css("display", "block");
    }else{
        $("#pass-error").css("display", "none");
        var users = JSON.parse(localStorage.getItem("users"));
        var user = {
            username, password
        }

        var is_user = false;
        for(var i=0;i<=users.length-1;i++) {
            if(users[i].username == username){
                $("#user-error").css("display", "block");
                is_user=true;
                break;
            }
        }

        if(!is_user) {
            users.push(user);
            localStorage.setItem('users', JSON.stringify(users));
            $("#signup-form").remove();
            $("#signup-success").css("display", "block");
        }
    }
});